<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::group(['middleware' => 'auth'], function() {

    Route::GET('/', function () {
        return Redirect::route('wallet.index');
    });

    Route::resource('wallet','WalletController',[
        'only' => ['show','index']
    ]);

    Route::resource('users','UsersController');
    Route::resource('faq','FaqController');
    Route::resource('accounts','AccountsController');
    Route::resource('bankacc','BankAccountsController');
    Route::resource('drafts','DraftsController');
    Route::resource('deposits','DepositsController');
});
