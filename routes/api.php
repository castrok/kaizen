<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function() {

    Route::resource('wallet','API\V1\WalletApiController',[
        'only' => ['index','show']
    ]);

    Route::resource('deposit','API\V1\DepositApiController',[
        'only' => ['show']
    ]);

    Route::resource('draft','API\V1\DraftApiController',[
        'only' => ['show']
    ]);

    Route::resource('income','API\V1\IncomeApiController',[
        'only' => ['show']
    ]);

    Route::resource('user','API\V1\UserApiController',[
        'only' => ['show']
    ]);

});
