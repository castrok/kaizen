$(window).on('load', function() {
    $("div[class='se-pre-con']").fadeOut("fast");
});

$(document).ready(function () {
    const swalWithBootstrapButtons = swal.mixin({
        showCancelButton: true,
        confirmButtonText: 'SIM !',
        cancelButtonText: 'NÃO !',
        confirmButtonColor: '#4caf50',
        cancelButtonColor: '#f44336',
        focusCancel: true,
    });

    $("input[id='cpf']").mask("999.999.999-99").on('blur',function(){
        alert('VERIFICA SE JA EXISTE O CPF NO SISTEMA !');
    });

    let data = new Date();
    let fileName = window.location.pathname.toLocaleUpperCase() + "  " + data.getDate() + "-" + data.getMonth() + "-" + data.getFullYear();
    $(".bs-dataTables").DataTable({
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
        },
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [{
            extend: 'collection',
            text: 'EXPORTAR',
            buttons: [{
                text: 'IMPRIMIR',
                extend: 'print',
                title: fileName,
                customize: function (win) {
                    $(win.document.body).css('font-size', '12pt');
                    $(win.document.body).find('table').addClass('compact').css('font-family', 'courier-new');
                },
                autoPrint: false,
                exportOptions: {
                    columns: ':visible'
                },
            },

                {
                    text: 'EXCEL',
                    extend: 'excelHtml5',
                    title: fileName,
                    exportOptions: {
                        columns: ':visible'
                    },
                },
                {
                    text: 'PDF',
                    extend: 'pdfHtml5',
                    title: fileName,
                    exportOptions: {
                        columns: ':visible'
                    },
                },
            ]
        },

            {
                text: 'COLUNAS',
                extend: 'colvis',
                columnText: function (dt, idx, title) {
                    return (idx + 1) + ': ' + title;
                },
            },
        ],
        select: true
    });

    let deposits_values = [];
    let drafts_values = [];
    let incomes_values = [];

    $.ajax({
        url: $('base[id="base"]').attr('content') + '/api/v1/wallet/' + $('meta[name="account_id"]').attr('content'),
        headers: {

        },
        type: 'GET',

        success: function (data) {

            $.each(data.deposits, function (index, value) {
                deposits_values[value.created_month - 1] = value.deposit_value;
            });
            $.each(deposits_values, function (index, value) {
                if (!value) {
                    deposits_values[index] = 0;
                }
            });

            $.each(data.drafts, function (index, value) {
                drafts_values[value.created_month - 1] = value.draft_value;
            });
            $.each(drafts_values, function (index, value) {
                if (!value) {
                    drafts_values[index] = 0;
                }
            });

            $.each(data.incomes, function (index, value) {
                incomes_values[value.created_month - 1] = value.income_value;
            });
            $.each(incomes_values, function (index, value) {
                if (!value) {
                    incomes_values[index] = 0;
                }
            });

            let lineData = {
                labels: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'setembro', 'Outubro', 'Novembro', 'Dezembro'],
                datasets: [{
                    label: "Depósitos",
                    backgroundColor: 'rgba(26,179,148,0.5)',
                    borderColor: 'rgba(26,179,148,0.7)',
                    pointBackgroundColor: "#1AB394",
                    pointBorderColor: "#FFF",
                    data: deposits_values,

                }, {
                    label: "Saques",
                    backgroundColor: '#5A6AFA',
                    borderColor: '#0819B3',
                    pointBackgroundColor: "#5A6AFA",
                    pointBorderColor: "#FFF",
                    data: drafts_values,
                }, {
                    label: "Rendimentos",
                    backgroundColor: '#ff5653',
                    borderColor: "#FF110A",
                    pointBackgroundColor: "#ff5653",
                    pointBorderColor: "#FFF",
                    data: incomes_values,
                }]
            };
            let lineOptions = {
                responsive: true,
            };
            new Chart($("canvas[id='lineChart']"), {
                type: 'line', data: lineData, options: lineOptions
            });
        },
        error: function (data) {
            console.log(data);
        },
    });

    $("a[id='receipt']").on('click', function () {
        swal.showLoading();
        Swal.fire({
            imageUrl: $(this).data('show_proof'),
            animation: false,
        });
    });
    $("a[id='logoff']").on('click', function () {
        swalWithBootstrapButtons.fire({
            title: 'CONFIRMAR LOGOUT ?',
            text: "Será necessário realizar o login novamente no proximo acesso !",
            type: 'warning',
        }).then((result) => {
            if (result.value) {
                swal.fire({
                    title: 'Saindo...',
                    animation: false,
                    customClass: 'animated zoomIn',
                    allowOutsideClick: false,
                });
                swal.showLoading();
                $.ajax({
                    url: $('base[id="base"]').attr('content') + '/logout',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    success: function () {
                        window.location.reload();
                        console.log('Usuário Deslogado!!')
                    },
                    error: function () {
                        swal.fire({
                            title: 'FALHA AO REQUISITAR O LOGOUT!!',
                            html: '<strong>INFORME ESTE ERRO AO ADMINISTRADOR DO SISTEMA</strong>',
                            type: 'warning',
                            animation: false,
                            customClass: 'animated zoomIn'
                        }).then(okay => {
                            if (okay) {
                                window.location.href = $('base[id="base"]').attr('content');
                            }
                        });
                    }
                });
            }
        });
    });
    $("a[id='deletar']").on('click', function () {
        swalWithBootstrapButtons.fire({
            title: 'Tem certeza que quer deletar ?',
            text: "Não é possivel desfazer esta ação",
            type: 'warning',
        }).then((result) => {
            if (result.value) {
                swal.fire({
                    type: 'info',
                    title: 'DELETANDO !',
                    text: 'Aguarde o final da operação, isto pode demorar um pouco!',
                    allowOutsideClick: false,
                });
                swal.showLoading();
                $.ajax({
                    url: $('base[id="base"]').attr('content') + $(this).data('url'),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'DELETE',
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        if (data == true) {
                            swal.fire({
                                title: 'DELETADO COM SUCESSO',
                                type: 'success',
                                animation: false,
                                customClass: 'animated zoomIn',
                            }).then(okay => {
                                if (okay) {
                                    window.location.reload();
                                }
                            });
                        } else {
                            swal.fire({
                                title: 'FALHA AO DELETAR',
                                text: 'Informe ao administrador',
                                type: 'error',
                                animation: false,
                                customClass: 'animated zoomIn',
                            }).then(okay => {
                                if (okay) {
                                    window.location.reload();
                                }
                            });
                        }
                    },
                    error: function (data) {
                        console.log(data);
                        swal.fire({
                            title: 'INFORME AO ADMINISTRADOR DO SISTEMA !!',
                            type: 'warning',
                            animation: false,
                            customClass: 'animated zoomIn'
                        });
                    },
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swal.fire({
                    title: 'CANCELADO',
                    text: 'Remoção cancelada !',
                    type: 'info',
                });
            }
        });
    });

    $("a[id='deposit_info']").on('click', function () {
        swal.showLoading();
        $.ajax({
            url: $('base[id="base"]').attr('content') + '/api/v1/deposit/' + $(this).data('deposit_id'),

            type: 'GET',
            success: function (data) {
                swal.fire({
                    title: data.deposit_informative,
                    html: "<div class='row'><div class='container-fluid text-left'>" +
                        "<b>ID: </b>" + data.deposit_id + "</br>" +
                        "<b>Valor: </b>R$ " + data.deposit_value.toFixed(2) + "</br>" +
                        "<hr />" +
                        "<b>DEPOSITO EFETIVADO EM: </b>" + $.format.date( data.created_at, 'dd/MM/yyyy H:mm:ss') + "<br>" +
                        "<b>ULTIMA ALTERAÇÃO: </b>" + $.format.date( data.updated_at, 'dd/MM/yyyy H:mm:ss') + "<br>" +
                        "</div></div>",
                    type: 'success',
                    animation: false,
                    customClass: 'animated zoomIn'
                });
            },
            error: function () {
                swal.fire({
                    title: 'FALHA AO BUSCAR DADOS!!',
                    html: '<strong>INFORME ESTE ERRO AO ADMINISTRADOR DO SISTEMA</strong>',
                    type: 'warning',
                    animation: false,
                    customClass: 'animated zoomIn'
                }).then(okay => {
                    if (okay) {
                        window.location.href = $('base[id="base"]').attr('content');
                    }
                });
            }
        });
    });
    $("a[id='draft_info']").on('click', function () {
        swal.showLoading();
        $.ajax({
            url: $('base[id="base"]').attr('content') + '/api/v1/draft/' + $(this).data('draft_id'),

            type: 'GET',
            success: function (data) {
                swal.fire({
                    title: data.draft_informative,
                    html: "<div class='row'><div class='container-fluid text-left'>" +
                        "<b>ID: </b>" + data.draft_id + "</br>" +
                        "<b>Valor: </b>R$ " + data.draft_value.toFixed(2) + "</br>" +
                        "<hr />" +
                        "<b>SAQUE EFETIVADO EM: </b>" + $.format.date( data.created_at, 'dd/MM/yyyy H:mm:ss') + "<br>" +
                        "<b>ULTIMA ALTERAÇÃO: </b>" + $.format.date( data.updated_at, 'dd/MM/yyyy H:mm:ss') + "<br>" +
                        "</div></div>",
                    type: 'success',
                    animation: false,
                    customClass: 'animated zoomIn'
                });
            },
            error: function () {
                swal.fire({
                    title: 'FALHA AO BUSCAR DADOS!!',
                    html: '<strong>INFORME ESTE ERRO AO ADMINISTRADOR DO SISTEMA</strong>',
                    type: 'warning',
                    animation: false,
                    customClass: 'animated zoomIn'
                }).then(okay => {
                    if (okay) {
                        window.location.href = $('base[id="base"]').attr('content');
                    }
                });
            }
        });
    });
    $("a[id='income_info']").on('click', function () {
        swal.showLoading();
        $.ajax({
            url: $('base[id="base"]').attr('content') + '/api/v1/income/' + $(this).data('income_id'),

            type: 'GET',
            success: function (data) {
                swal.fire({
                    title: data.income_informative,
                    html: "<div class='row'><div class='container-fluid text-left'>" +
                        "<b>ID: </b>" + data.income_id + "</br>" +
                        "<b>Valor: </b>R$ " + data.income_value.toFixed(2) + "</br>" +
                        "<hr />" +
                        "<b>RENDIMENTO EFETIVADO EM: </b>" + $.format.date( data.created_at, 'dd/MM/yyyy H:mm:ss') + "<br>" +
                        "<b>ULTIMA ALTERAÇÃO: </b>" + $.format.date( data.updated_at, 'dd/MM/yyyy H:mm:ss') + "<br>" +
                        "</div></div>",
                    type: 'success',
                    animation: false,
                    customClass: 'animated zoomIn'
                });
            },
            error: function () {
                swal.fire({
                    title: 'FALHA AO BUSCAR DADOS!!',
                    html: '<strong>INFORME ESTE ERRO AO ADMINISTRADOR DO SISTEMA</strong>',
                    type: 'warning',
                    animation: false,
                    customClass: 'animated zoomIn'
                }).then(okay => {
                    if (okay) {
                        window.location.href = $('base[id="base"]').attr('content');
                    }
                });
            }
        });
    });
    $("a[id='user_info']").on('click', function () {
        swal.showLoading();
        $.ajax({
            url: $('base[id="base"]').attr('content') + '/api/v1/user/' + $(this).data('id'),

            type: 'GET',
            success: function (data) {
                swal.fire({
                    title: data.name,
                    html: "<div class='row'><div class='container-fluid text-left'>" +
                        "<b>ID: </b>" + data.id + "</br>" +
                        "<hr />" +
                        "<b>CADASTRADO EM: </b>" + $.format.date( data.created_at, 'dd/MM/yyyy H:mm:ss') + "<br>" +
                        "<b>ULTIMA ALTERAÇÃO: </b>" + $.format.date( data.updated_at, 'dd/MM/yyyy H:mm:ss') + "<br>" +
                        "</div></div>",
                    type: 'success',
                    animation: false,
                    customClass: 'animated zoomIn'
                });
            },
            error: function () {
                swal.fire({
                    title: 'FALHA AO BUSCAR DADOS!!',
                    html: '<strong>INFORME ESTE ERRO AO ADMINISTRADOR DO SISTEMA</strong>',
                    type: 'warning',
                    animation: false,
                    customClass: 'animated zoomIn'
                }).then(okay => {
                    if (okay) {
                        window.location.href = $('base[id="base"]').attr('content');
                    }
                });
            }
        });
    });

    $('[data-toggle="tooltip"]').tooltip();
}); //END READY FUNCTION
