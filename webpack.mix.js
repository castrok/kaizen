const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.scripts([
    'public/sources/js/cores/jquery-3.4.1.js',
], 'public/js/cores/jquery-3.4.1.min.js'); //JQUERY JS

mix.scripts([
    'public/sources/css/cores/bootstrap.css',
], 'public/css/cores/bootstrap.min.css'); //BOOTSTRAP CSS
mix.scripts([
    'public/sources/js/cores/bootstrap.js',
], 'public/js/cores/bootstrap.min.js'); //BOOTSTRAP JS

mix.scripts([
    'public/sources/css/plugins/sweetalert2.css',
], 'public/css/cores/sweetalert2.min.css'); //SWEET ALERT 2 CSS
mix.scripts([
    'public/sources/js/plugins/sweetalert2.all.js',
], 'public/js/cores/sweetalert2.min.js'); //SWEET ALERT 2 JS

mix.scripts([
    'public/sources/css/cores/inspina.css',
], 'public/css/cores/inspina.min.css'); //TEMPLATE CORE CSS
mix.scripts([
    'public/sources/js/cores/inspina.js',
], 'public/js/cores/inspina.min.js'); //TEMPLATE CORE JS

mix.scripts([
    'public/sources/js/plugins/DataTables/datatables.js',
], 'public/js/plugins/DataTables/datatables.min.js'); //DATA TABLES CORE CSS
mix.scripts([
    'public/sources/css/plugins/DataTables/datatables.css',
], 'public/css/plugins/DataTables/datatables.min.css');  //DATA TABLES CORE JS

mix.scripts([
    'public/sources/css/cores/customInspina.css',
], 'public/css/cores/customInspina.min.css'); //TEMPLATE CUSTOM CSS
mix.scripts([
    'public/sources/js/cores/customInspina.js',
], 'public/js/cores/customInspina.min.js'); //TEMPLATE CUSTOM JS

mix.scripts([
    'public/sources/js/plugins/bootstrapValidator.js',
], 'public/js/plugins/bootstrapValidator.min.js');  //DATA TABLES CORE JS
mix.scripts([
    'public/sources/js/plugins/maskedinput.js',
], 'public/js/plugins/maskedinput.min.js'); //MASK INPUT JS


