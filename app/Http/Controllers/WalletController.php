<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Helpers\AccountsHelperController;
use App\Models\Account;
use App\services\AccountsService;
use App\services\DepositsService;
use App\services\DraftsService;
use App\services\IncomesService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Response;
use Exception;

class WalletController extends  Controller {

    /**
     * Create a new instance of an Account
     * @author Gilson Vieira Castro Júnior <castrok@live.com / (61) 9 9209-6171>
     * UsersController constructor.
     */
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @author Gilson Vieira Castro Júnior <castrok@live.com / (61) 9 9209-6171>
     * @return Response
     * @throws AuthorizationException
     */
    public function index() {
        $this->authorize('accounts_index');
        try {
            $accounts = AccountsService::show_authenticated();
            $deposits = DepositsService::show_authenticated();
            $drafts = DraftsService::show_authenticated();
            $incomes = IncomesService::show_authenticated();

            $accounts->titre_name = Account::query()->select(['users.name']) ->join('users','users.id','accounts.titre')
                ->where('accounts.account_id', $accounts->account_id)->firstOrFail()->name;

            $accounts->balance_expected = AccountsHelperController::balance_expected($accounts->balance_atual, $accounts->income_tax);
            $accounts->next_month_total_ballance_expected = AccountsHelperController::next_month_total_ballance_expected($accounts->balance_atual, $accounts->income_tax);

        }
        catch (Exception $exception) {
            report($exception);
            return redirect()->back();
        }
        return view('kaizen.wallet.investor',compact('accounts','deposits','drafts', 'incomes'));
    }

    /**
     * Display the specified resource.
     * @author Gilson Vieira Castro Júnior <castrok@live.com / (61) 9 9209-6171>
     * @param int $id
     * @return Response
     * @throws AuthorizationException
     */
    public function show($id) {
        $this->authorize('accounts_show');
        try {
            $accounts = AccountsService::show($id);
            $deposits = DepositsService::show_by_account($id);
            $drafts = DraftsService::show_by_account($id);
            $incomes = IncomesService::show_by_account($id);

            $accounts->titre_name = Account::query()->select(['users.name']) ->join('users','users.id','accounts.titre')
                ->where('accounts.account_id',$id)->firstOrFail()->name;

            $accounts->balance_expected = AccountsHelperController::balance_expected($accounts->balance_atual, $accounts->income_tax);
            $accounts->next_month_total_ballance_expected = AccountsHelperController::next_month_total_ballance_expected($accounts->balance_atual, $accounts->income_tax);

        }

        catch (Exception $exception) {
            report($exception);
            return redirect()->back();
        }
        return view('kaizen.wallet.investor',compact('accounts','deposits','drafts', 'incomes'));
    }
}
