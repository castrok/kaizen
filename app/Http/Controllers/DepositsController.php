<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Helpers\DraftsHelperController;
use App\Models\Account;
use App\Models\Deposit;
use App\Models\User;
use Gate;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\services\DepositsService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Auth\Access\AuthorizationException;
use App\Http\Controllers\Helpers\DepositsHelperController;

class DepositsController extends DepositsHelperController {

    /**
     * Display a listing of the resource.
     * @return Response
     * @throws AuthorizationException
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     */
    public function index() {
        $this->authorize('deposits_index');
        try {
            if (Gate::allows('investor')) {
                $deposits = DepositsService::show_authenticated();
            }
        }

        catch (Exception $exception) {
            report($exception);
        }

        finally {
            print '<pre>'; print_r($deposits->toArray()); die;
            #return view('kaizen.deposits.index' ,compact('deposits'));
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     * @throws AuthorizationException
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     */
    public function create() {
        $this->authorize('deposits_create');
        try {
            $deposits = new Account();
            $accounts = Account::query()
                ->select(['accounts.*','users.name as titre_name'])
                ->join('users','users.id','accounts.titre')->get();

        }
        catch (Exception $exception) {
            report($exception);
        }
        finally {
            return view('kaizen.deposits.form',compact('deposits','accounts'));
        }

    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     */
    public function store(Request $request) {
        $this->authorize('deposits_store');
        try {
            if ($request->hasFile('deposit_proof') && $request->file('deposit_proof')->isValid()) {

                $titre = User::find(Account::find($request->account)->titre); #ACCOUNT OWNER

                $receipt_name = DraftsHelperController::make_receipt_name("$request->deposit_value", $request->deposit_proof->extension(), $titre->cpf); #FILE NAME
                $receipt_path = DraftsHelperController::make_receipt_path($titre->rg); #FILEPATH

                Deposit::insert([
                    'deposit_value' => $request->deposit_value,
                    'account' => $request->account,
                    'deposit_proof' => (string) $receipt_path.$receipt_name,
                    'deposit_informative' => $request->deposit_informative,
                ]); #DATA PERSISTENCE

                $upload = $request->deposit_proof->storeAs((string) $receipt_path, (string) $receipt_name);

                DB::commit();
            }
        }
        catch (Exception $exception) {
            report($exception);
            DB::rollBack();
            return redirect()->back()->withInput();
        }

        finally {
            return redirect()->route('wallet.index');
        }
    }

    /**
     * Display the specified resource.
     * @param int $id
     * @return Response
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     */
    public function show($id)
    {
        $this->authorize('deposits_show');
        try {
        } catch (Exception $exception) {
            report($exception);
        } finally {
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     */
    public function edit($id)
    {
        $this->authorize('deposits_edit');
        try {
        } catch (Exception $exception) {
            report($exception);
        } finally {
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     * @throws AuthorizationException
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     */
    public function update(Request $request, $id) {
        $this->authorize('deposits_update');
        try {

        }

        catch (Exception $exception) {
            report($exception);
            DB::rollBack();
            return redirect()->back()->withInput();
        }

        finally {

        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     */
    public function destroy($id)
    {
        $this->authorize('deposits_destroy');
        try {
        } catch (Exception $exception) {
            report($exception);
        } finally {
        }
    }
}
