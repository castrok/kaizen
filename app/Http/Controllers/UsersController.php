<?php

namespace App\Http\Controllers;

use App\Models\User;
use Auth;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersController extends Controller{

    /** Create a new instance of an User
     * @author Gilson Vieira Castro Júnior <castrok@live.com / (61) 9 9209-6171>
     * UsersController constructor.
     * @param $users
     */
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     * @throws AuthorizationException
     * @author Gilson Vieira Castro Júnior <castrok@live.com / (61) 9 9209-6171>
     */
    public function index(){
        $this->authorize('users_index');
        try{
            $users = User::all();
        }
        catch(Exception $exception){
            report($exception);
        }
        finally{
            return view('kaizen.users.index',compact('users'));
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     * @throws AuthorizationException
     * @author Gilson Vieira Castro Júnior <castrok@live.com / (61) 9 9209-6171>
     */
    public function create(){
        $this->authorize('users_create');
        try{
            $users = new User();
        }
        catch(Exception $exception){
            report($exception);
        }
        finally{
            return view('kaizen.users.form',compact('users'));
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     * @author Gilson Vieira Castro Júnior <castrok@live.com / (61) 9 9209-6171>
     */
    public function store(Request $request){
        $this->authorize('users_store');
        try{
            DB::beginTransaction();
            User::insert([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'rg' => $request->rg,
                'cpf' => $request->cpf,
            ]);
            DB::commit();
        }
        catch(Exception $exception){
            DB::rollBack();
            report($exception);
        }
        finally{
            return redirect()->route('users.index');
        }
    }

    /**
     * Display the specified resource.
     * @param int $id
     * @return Response
     * @throws AuthorizationException
     * @author Gilson Vieira Castro Júnior <castrok@live.com / (61) 9 9209-6171>
     */
    public function show($id){
        $this->authorize('users_show');
        try{
            if (Gate::allows('investor')) {
                $id = Auth::id();
            }
            $users = User::findOrFail($id);
        }
        catch(Exception $exception){
            report($exception);
        }
        finally{
            return view('kaizen.users.show',compact('users'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     * @throws AuthorizationException
     * @author Gilson Vieira Castro Júnior <castrok@live.com / (61) 9 9209-6171>
     */
    public function edit($id){
        $this->authorize('users_edit');
        try{
            if (Gate::allows('investor')) {
                $id = Auth::id();
            }
            $users = User::findOrFail($id);
        }
        catch(Exception $exception){
            report($exception);
        }
        finally{
            return view('kaizen.users.form',compact('users'));
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     * @throws AuthorizationException
     * @author Gilson Vieira Castro Júnior <castrok@live.com / (61) 9 9209-6171>
     */
    public function update(Request $request, $id){
        $this->authorize('users_update');
        try{
            DB::beginTransaction();

            $requestInputs = [
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'api_token' => Str::random(60),
                'rg' => $request->rg,
                'cpf' => $request->cpf,
            ];

            if(empty($request->password))
                unset($requestInputs['password']);

            $users = User::where('id', $id)->update($requestInputs);

            DB::commit();
        }
        catch(Exception $exception){
            DB::rollBack();
            report($exception);
        }
        finally{
            return redirect()->route('users.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     * @throws AuthorizationException
     * @author Gilson Vieira Castro Júnior <castrok@live.com / (61) 9 9209-6171>
     */
    public function destroy($id){
        $this->authorize('users_destroy');
        try{
            $users = User::destroy($id);
        }
        catch(Exception $exception){
            report($exception);
        }
        finally{
            return response()->json($users);
        }
    }
}
/*$permissions = Permission::all();
    foreach ($permissions as $permission){
    $PermissionsRoleid = PermissionRole::insertGetId([
        'permission' => $permission->permission_id,
        'role' => 1,
    ]);
    print "<pre>"; print_r($PermissionsRoleid);
}*/
