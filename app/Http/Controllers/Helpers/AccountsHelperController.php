<?php

namespace App\Http\Controllers\Helpers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

abstract class AccountsHelperController extends Controller {

    /**
     * Calcula o rendimento.
     * @author Gilson Vieira Castro Júnior <castrok@live.com / (61) 9 9209-6171>
     * @param  float $income_tax
     * @param float $income_value
     * @return float
     */
    public static function balance_expected(float $income_value, float $income_tax){
        return $income_value / 100 * $income_tax;
    }

    /**
     * Calcula o saldo total do mês seguinte.
     * @param float $income_value
     * @param float $income_tax
     * @return float
     * @author Gilson Vieira Castro Júnior <castrok@live.com / (61) 9 9209-6171>
     */
    public static function next_month_total_ballance_expected(float $income_value, float $income_tax) {
        return $income_value + self::balance_expected($income_value ,$income_tax);
    }
}
