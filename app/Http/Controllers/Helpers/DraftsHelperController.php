<?php

namespace App\Http\Controllers\Helpers;

use App\Models\User;
use Exception;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\Types\Integer;

abstract class DraftsHelperController extends Controller {

    /**
     * @param string $value
     * @param string $extension
     * @return string
     */
    public static function make_receipt_name(string $value, string $extension, string $cpf) {
        try {
            $account = Auth::user()->accounts->first()->account_id;

            $value = str_replace(['.', ','], '', $value);
            $timestamp = date('Ydm');
            $registration = str_replace(['.', ',','-'], '', $cpf);

            $receipt_name = "{$account}_{$registration}_{$timestamp}_{$value}.{$extension}";

        }
        catch (Exception $exception) {
            report($exception);
        }

        return "$receipt_name";
    }

    /**
     * @param string $registration
     * @return string
     */
    public static function make_receipt_path($rg){
        return "$rg/Receipts/Drafts/";
    }
}
