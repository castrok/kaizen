<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Helpers\DepositsHelperController;
use App\Http\Controllers\Helpers\DraftsHelperController;
use App\Models\Account;
use App\Models\Draft;
use App\Models\User;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DraftsController extends Controller {

    /**
     * DraftsController constructor.
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     * @throws AuthorizationException
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     */
    public function index() {
        $this->authorize('drafts_index');
        try {
            $drafts = Draft::all();
        }
        catch (Exception $exception) {
            report($exception);
        }
        finally {
            return view('kaizen.drafts.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     * @throws AuthorizationException
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     */
    public function create() {
        $this->authorize('drafts_create');
        try {
            $drafts = new Account();
            $accounts = Account::query()->select(['accounts.*','users.name as titre_name'])
                ->join('users','users.id','accounts.titre')->get();

        } catch (Exception $exception) {
            report($exception);
        }

        finally {
            return view('kaizen.drafts.form',compact('drafts','accounts'));
        }

    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     */
    public function store(Request $request) {
        $this->authorize('drafts_store');
        try {
            if ($request->hasFile('draft_proof') && $request->file('draft_proof')->isValid()) {

                $titre = User::find(Account::find($request->account)->titre); #ACCOUNT OWNER

                $receipt_name = DepositsHelperController::make_receipt_name("$request->draft_value", $request->draft_proof->extension(), $titre->cpf); #FILE NAME
                $receipt_path = DepositsHelperController::make_receipt_path($titre->rg); #FILEPATH

                Draft::insert([
                    'draft_value' => $request->draft_value,
                    'account' => $request->account,
                    'draft_proof' => (string) $receipt_path.$receipt_name,
                    'draft_informative' => $request->draft_informative,
                ]); #DATA PERSISTENCE

                $upload = $request->draft_proof->storeAs((string) $receipt_path, (string) $receipt_name);

                DB::commit();
            }
        }

        catch (Exception $exception) {
            report($exception);
            DB::rollBack();
            return redirect()->back()->withInput();
        }

        finally {
            return redirect()->route('wallet.index');
        }
    }

    /**
     * Display the specified resource.
     * @param int $id
     * @return Response
     * @throws AuthorizationException
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     */
    public function show($id) {
        $this->authorize('drafts_show');
        try {}
        catch (Exception $exception) {
            report($exception);
        }
        finally {}
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     * @throws AuthorizationException
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     */
    public function edit($id) {
        $this->authorize('drafts_edit');
        try {}
        catch (Exception $exception) {
            report($exception);
        }
        finally {}
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     * @throws AuthorizationException
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     */
    public function update(Request $request, $id) {
        $this->authorize('drafts_update');
        try {}
        catch (Exception $exception) {
            report($exception);
        }
        finally {}
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     * @throws AuthorizationException
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     */
    public function destroy($id) {
        $this->authorize('drafts_destroy');
        try {}
        catch (Exception $exception) {
            report($exception);
        }
        finally {}
    }
}

