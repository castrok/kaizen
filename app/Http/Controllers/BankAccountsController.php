<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\User;
use App\Models\BankAccount;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\AuthorizationException;

class BankAccountsController extends Controller {

    /**
     * Display a listing of the resource.
     * @return Response
     * @throws AuthorizationException
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     */
    public function index() {
        $this->authorize('bankacc_index');
        try {
            $bankaccounts = BankAccount::query()->select(['bank_accounts.*','users.name as bank_acc_titre'])
                ->join('users','users.id','bank_accounts.bank_account_titre');

            if (!Gate::allows('investor')) {
                $bankaccounts->where('bank_accounts.bank_account_titre', Auth::id());
            }

            $bankaccounts = $bankaccounts->get();
        }
        catch (Exception $exception) {
            report($exception);
        }
        finally {
            return view('kaizen.bankaccounts.index',compact('bankaccounts'));
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     * @throws AuthorizationException
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     */
    public function create() {
        $this->authorize('bankacc_create');
        try {
            $bankaccounts = new BankAccount();
            $bankaccounts->bank_acc_titre = User::all();
        }
        catch (Exception $exception) {
            report($exception);
        }
        finally {
            return view('kaizen.bankaccounts.form',compact('bankaccounts'));
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     */
    public function store(Request $request){
        $this->authorize('bankacc_store', $request);
        try {
            DB::beginTransaction();
            BankAccount::insert([
                'bank_account_titre' => $request->bank_account_titre,
                'agency' => $request->agency,
                'account_type' => $request->account_type,
                'bank_name' => $request->bank_name,
                'account_number' => $request->account_number,
            ]);
            DB::commit();
        }
        catch (Exception $exception) {
            report($exception);
            DB::rollBack();
        }
        finally {
            return redirect()->route('bankacc.index');
        }
    }

    /**
     * Display the specified resource.
     * @param int $id
     * @return Response
     * @throws AuthorizationException
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     */
    public function show($id) {
        $this->authorize('bankacc_show');
        try {
            $bankaccounts = BankAccount::find($id);
        }
        catch (Exception $exception) {
            report($exception);
        }
        finally {
            return view('kaizen.bankaccounts.show',compact('bankaccounts'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     * @throws AuthorizationException
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     */
    public function edit($id){
        $this->authorize('bankacc_edit');
        try {
            $bankaccounts = BankAccount::find($id);
            $bankaccounts->bank_acc_titre = User::all();
        }
        catch (Exception $exception) {
            report($exception);
        }
        finally {
            return view('kaizen.bankaccounts.form',compact('bankaccounts'));
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     * @throws AuthorizationException
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     */
    public function update(Request $request, $id){
        $this->authorize('bankacc_update',$request);
        try {
            DB::beginTransaction();
            BankAccount::where('bank_account_id',$id)->update([
                'bank_account_titre' => $request->bank_account_titre,
                'agency' => $request->agency,
                'account_type' => $request->account_type,
                'bank_name' => $request->bank_name,
                'account_number' => $request->account_number,
            ]);
            DB::commit();
        }
        catch (Exception $exception) {
            report($exception);
            DB::rollBack();
        }
        finally {
            return redirect()->route('bankacc.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     * @throws AuthorizationException
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     */
    public function destroy($id){
        $this->authorize('bankacc_destroy');
        try {
            BankAccount::destroy($id);
        }
        catch (Exception $exception) {
            report($exception);
        }
        finally {
            return redirect()->route('bankacc.index');
        }
    }
}
