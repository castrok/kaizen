<?php

namespace App\Http\Controllers\API\V1;

use App\services\DepositsService;
use App\services\DraftsService;
use App\services\IncomesService;
use Exception;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class WalletApiController extends Controller {

    /**
     * Display a listing of the resource.
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     * @return Response
     */
    public function index() {
        try {
            $data = [
                'deposits' => DepositsService::show_authenticated(),
                'drafts' => DraftsService::show_authenticated(),
                'incomes' => IncomesService::show_authenticated(),
            ];

            foreach ($data['deposits'] as $deposit) {
                $deposit->created_month = (int) strftime('%m', strtotime($deposit->created_at));
            }

            foreach ($data['drafts'] as $draft) {
                $draft->created_month = (int) strftime('%m', strtotime($draft->created_at));
            }

            foreach ($data['incomes'] as $income) {
                $income->created_month = (int) strftime('%m', strtotime($income->created_at));
            }
        }

        catch (Exception $exception) {
            report($exception);
        }

        finally {
            return Response()->json($data);
        }
    }

    /**
     * Display the specified resource.
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        try {
            $data = [
                'deposits' => DepositsService::show_by_account($id),
                'drafts' => DraftsService::show_by_account($id),
                'incomes' => IncomesService::show_by_account($id),
            ];

            foreach ($data['deposits'] as $deposit) {
                $deposit->created_month = (int) strftime('%m', strtotime($deposit->created_at));
            }
            foreach ($data['drafts'] as $draft) {
                $draft->created_month = (int) strftime('%m', strtotime($draft->created_at));
            }
            foreach ($data['incomes'] as $income) {
                $income->created_month = (int) strftime('%m', strtotime($income->created_at));
            }
        }

        catch (Exception $exception) {
            report($exception);
        }

        finally {
            return Response()->json($data);
        }
    }
}
