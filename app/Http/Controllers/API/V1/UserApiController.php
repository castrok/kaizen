<?php

namespace App\Http\Controllers\API\V1;

use Exception;
use App\Models\User;
use App\Http\Controllers\Controller;

class UserApiController extends Controller {

    /**
     * Display the specified resource.
     * @author Gilson Vieira Castro Júnior <castrok@live.com +55 (61) 9 9209-6171>
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        try {
            $user = User::find($id);
        }
        catch (Exception $exception) {
            report($exception);
            return response()->json("error");
        }
        return $user;
    }

}
