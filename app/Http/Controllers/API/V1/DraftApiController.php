<?php


namespace App\Http\Controllers\API\V1;


use App\services\DraftsService;
use Exception;
use Illuminate\Http\Response;

class DraftApiController {

    /**
     * Display the specified resource.
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        try {
            $draft = DraftsService::show($id);
        }

        catch (Exception $exception) {
            report($exception);
        }

        finally {
            return Response()->json($draft);
        }
    }

}
