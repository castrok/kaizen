<?php

namespace App\Http\Controllers;

use App\Models\Faq;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class FaqController extends Controller
{
    /**
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(){
        try {

        }
        catch (Exception $exception) {
            report($exception);
        }
        finally {
            return view('kaizen.faq.index');
        }
    }

    /**
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(){
        try { }
        catch (Exception $exception) {
            report($exception);
        }
        finally {}
    }

    /**
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request){
        $this->authorize('faq_store', $request);
        try {
            DB::beginTransaction();
            Faq::insert([
                'question' => $request->question,
            ]);
            DB::commit();
        }
        catch (Exception $exception) {
            report($exception);
            DB::rollBack();
        }
        finally {
            return redirect()->route('faq.index');
        }
    }

    /**
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id){
        try { }
        catch (Exception $exception) {
            report($exception);
        }
        finally {}
    }

    /**
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id){
        try { }
        catch (Exception $exception) {
            report($exception);
        }
        finally {}
    }

    /**
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id){
        try { }
        catch (Exception $exception) {
            report($exception);
        }
        finally {}
    }

    /**
     * @author Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id){
        try { }
        catch (Exception $exception) {
            report($exception);
        }
        finally {}
    }
}
