<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Helpers\AccountsHelperController;
use App\Models\Account;
use App\Models\User;
use Auth;
use DB;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;

class AccountsController extends AccountsHelperController {

    /** Create a new instance of an Account
     * @author Gilson Vieira Castro Júnior <castrok@live.com / (61) 9 9209-6171>
     * UsersController constructor.
     */
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     * @return Response
     * @throws AuthorizationException
     */
    public function index() {
        $this->authorize('accounts_index');
        try {
            $accounts = Account::query()
                ->select(['accounts.*','users.name as titre_name'])
                ->join('users','users.id','accounts.titre')->get();
        }
        catch (Exception $exception) {
            report($exception);
        }
        finally {
            return view('kaizen.accounts.index',compact('accounts'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        try{
            $accounts = new Account();
            $accounts->titres = User::all();
        }
        catch (Exception $exception) {
            report($exception);
        }
        finally {
            return view('kaizen.accounts.form',compact('accounts'));
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     * @author Gilson Vieira Castro Júnior < castrok@live.com / (61) 9 9209-6171>
     */
    public function store(Request $request) {
        $this->authorize('accounts_store');
        try{
            DB::beginTransaction();
            Account::insert([
                'titre' => $request->titre,
                'income_tax' => $request->income_tax,
                'balance_atual' => $request->balance_atual,
            ]);
            DB::commit();
        }
        catch (Exception $exception) {
            report($exception);
            DB::rollBack();
        }
        finally {
            return redirect()->route('accounts.index');
        }
    }

    /**
     * Display the specified resource.
     * @author Gilson Vieira Castro Júnior <castrok@live.com / (61) 9 9209-6171>
     * @param int $id
     * @return Response
     * @throws AuthorizationException
     */
    public function show($id){
        $this->authorize('accounts_show');
        try {
            if (Gate::allows('investor')) {
                $id = Auth::id();
            }

            $accounts = Account::where('titre', $id)->firstOrFail();

        }
        catch(Exception $exception){
            report($exception);
        }
        finally{
            return view('kaizen.accounts.show',compact('accounts'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id){
        try {
            $accounts = Account::findOrFail($id);
            $accounts->titres = User::all();
        }
        catch(Exception $exception){
            report($exception);
        }
        finally {
            return view('kaizen.accounts.form',compact('accounts'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id){
        try{
            DB::beginTransaction();
            Account::where('id', $id)->update([
                'titre' => $request->titre,
                'income_tax' => $request->income_tax,
                'balance_atual' => $request->balance_atual,
            ]);
            DB::commit();
        }
        catch(Exception $exception){
            DB::rollBack();
            report($exception);
        }
        finally{
            return redirect()->route('accounts.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id){
        try {
            Account::destroy($id);
        }

        catch (Exception $exception) {
            report($exception);
        }

        finally {
            return redirect()->route('accounts.index');
        }
    }
}
