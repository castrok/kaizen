<?php

namespace App\Models;

class Deposit extends \App\Models\Base\Deposit
{
	protected $fillable = [
		'deposit_value',
		'account',
		'deposit_informative'
	];
}
