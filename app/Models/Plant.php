<?php

namespace App\Models;

class Plant extends \App\Models\Base\Plant
{
	protected $fillable = [
		'plant_name',
		'plant_panel_plant_panel'
	];
}
