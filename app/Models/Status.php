<?php

namespace App\Models;

class Status extends \App\Models\Base\Status{
	protected $fillable = [
		'plant',
		'moisture'
	];
}
