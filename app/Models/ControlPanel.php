<?php

namespace App\Models;

class ControlPanel extends \App\Models\Base\ControlPanel
{
	protected $fillable = [
		'owner'
	];
}
