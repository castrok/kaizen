<?php

namespace App\Models;

class Config extends \App\Models\Base\Config
{
	protected $fillable = [
		'irrigation_straight',
		'plant',
		'humidity',
		'config_informative'
	];
}
