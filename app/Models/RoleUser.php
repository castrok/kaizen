<?php

namespace App\Models;

class RoleUser extends \App\Models\Base\RoleUser
{
	protected $fillable = [
		'role',
		'user'
	];
}
