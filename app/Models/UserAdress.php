<?php

namespace App\Models;

class UserAdress extends \App\Models\Base\UserAdress
{
	protected $fillable = [
		'user_id',
		'zip_code',
		'city'
	];
}
