<?php

namespace App\Models;

class Draft extends \App\Models\Base\Draft
{
	protected $fillable = [
		'draft_value',
		'account',
		'bank_account',
		'draft_informative'
	];
}
