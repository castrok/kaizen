<?php

namespace App\Models;

class Faq extends \App\Models\Base\Faq
{
	protected $fillable = [
		'question',
		'answer'
	];
}
