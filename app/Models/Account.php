<?php

namespace App\Models;

class Account extends \App\Models\Base\Account
{
	protected $fillable = [
		'titre',
		'income_tax',
		'balance_atual'
	];
}
