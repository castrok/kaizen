<?php

namespace App\Models;

class BankAccount extends \App\Models\Base\BankAccount
{
	protected $fillable = [
		'bank_account_titre',
		'agency',
		'account_type',
		'bank_name',
		'account_number'
	];
}
