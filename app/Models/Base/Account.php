<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 02 Aug 2019 23:21:03 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Account
 * 
 * @property int $account_id
 * @property int $titre
 * @property float $income_tax
 * @property float $balance_atual
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $deposits
 * @property \Illuminate\Database\Eloquent\Collection $drafts
 *
 * @package App\Models\Base
 */
class Account extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $primaryKey = 'account_id';

	protected $casts = [
		'titre' => 'int',
		'income_tax' => 'float',
		'balance_atual' => 'float'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'titre');
	}

	public function deposits()
	{
		return $this->hasMany(\App\Models\Deposit::class, 'account');
	}

	public function drafts()
	{
		return $this->hasMany(\App\Models\Draft::class, 'account');
	}
}
