<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 05 Aug 2019 14:18:47 -0300.
 */

namespace App\Models\Base;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $cpf
 * @property string $rg
 * @property Carbon $email_verified_at
 * @property string $password
 * @property string $api_token
 * @property string $remember_token
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @property Collection $accounts
 * @property Collection $bank_accounts
 * @property Collection $roles
 *
 * @package App\Models\Base
 */
class User extends Authenticatable {

	use SoftDeletes;

	protected $dates = [
		'email_verified_at'
	];

	public function accounts()
	{
		return $this->hasMany(\App\Models\Account::class, 'titre');
	}

	public function bank_accounts()
	{
		return $this->hasMany(\App\Models\BankAccount::class, 'bank_account_titre');
	}

	public function roles()
	{
		return $this->belongsToMany(\App\Models\Role::class, 'role_users', 'user', 'role')
					->withPivot('role_user_id')
					->withTimestamps();
	}
}
