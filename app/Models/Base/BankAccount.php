<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 30 Jul 2019 16:51:49 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class BankAccount
 * 
 * @property int $bank_account_id
 * @property int $bank_account_titre
 * @property string $agency
 * @property string $account_type
 * @property string $bank_name
 * @property string $account_number
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models\Base
 */
class BankAccount extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $primaryKey = 'bank_account_id';

	protected $casts = [
		'bank_account_titre' => 'int'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'bank_account_titre');
	}
}
