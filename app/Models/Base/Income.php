<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 04 Aug 2019 16:51:07 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Income
 * 
 * @property int $income_id
 * @property float $income_value
 * @property int $account
 * @property string $income_informative
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 *
 * @package App\Models\Base
 */
class Income extends Eloquent
{
	protected $primaryKey = 'income_id';

	protected $casts = [
		'income_value' => 'float',
		'account' => 'int'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class, 'account');
	}
}
