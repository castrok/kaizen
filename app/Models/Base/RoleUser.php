<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 30 Jul 2019 00:32:32 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class RoleUser
 * 
 * @property int $role_user_id
 * @property int $role
 * @property int $user
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 *
 * @package App\Models\Base
 */
class RoleUser extends Eloquent
{
	protected $primaryKey = 'role_user_id';

	protected $casts = [
		'role' => 'int',
		'user' => 'int'
	];

	public function role()
	{
		return $this->belongsTo(\App\Models\Role::class, 'role');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'user');
	}
}
