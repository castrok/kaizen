<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 30 Jul 2019 00:32:32 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Role
 * 
 * @property int $role_id
 * @property string $role_name
 * @property string $role_informative
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $permissions
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models\Base
 */
class Role extends Eloquent
{
	protected $primaryKey = 'role_id';

	public function permissions()
	{
		return $this->belongsToMany(\App\Models\Permission::class, 'permission_roles', 'role', 'permission')
					->withPivot('permission_role_id');
	}

	public function users()
	{
		return $this->belongsToMany(\App\Models\User::class, 'role_users', 'role', 'user')
					->withPivot('role_user_id')
					->withTimestamps();
	}
}
