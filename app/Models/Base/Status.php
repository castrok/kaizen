<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 14 Jun 2019 00:47:00 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Status
 * 
 * @property int $status_id
 * @property int $plant
 * @property float $moisture
 * 
 *
 * @package App\Models\Base
 */
class Status extends Eloquent
{
	protected $table = 'status';
	protected $primaryKey = 'status_id';
	public $timestamps = false;

	protected $casts = [
		'plant' => 'int',
		'moisture' => 'float'
	];

	public function plant()
	{
		return $this->belongsTo(\App\Models\Plant::class, 'plant');
	}
}
