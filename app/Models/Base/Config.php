<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 31 May 2019 21:25:56 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Config
 * 
 * @property int $config_id
 * @property int $irrigation_straight
 * @property int $plant
 * @property int $humidity
 * @property string $config_informative
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 *
 * @package App\Models\Base
 */
class Config extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $primaryKey = 'config_id';

	protected $casts = [
		'irrigation_straight' => 'int',
		'plant' => 'int',
		'humidity' => 'int'
	];

	public function plant()
	{
		return $this->belongsTo(\App\Models\Plant::class, 'plant');
	}
}
