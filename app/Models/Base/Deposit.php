<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 09 Oct 2019 13:58:08 -0300.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Deposit
 * 
 * @property int $deposit_id
 * @property float $deposit_value
 * @property int $account
 * @property string $deposit_proof
 * @property string $deposit_informative
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 *
 * @package App\Models\Base
 */
class Deposit extends Eloquent
{
	protected $primaryKey = 'deposit_id';

	protected $casts = [
		'deposit_value' => 'float',
		'account' => 'int'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class, 'account');
	}
}
