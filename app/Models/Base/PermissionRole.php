<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 30 Jul 2019 00:32:32 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PermissionRole
 * 
 * @property int $permission_role_id
 * @property int $permission
 * @property int $role
 * 
 *
 * @package App\Models\Base
 */
class PermissionRole extends Eloquent
{
	protected $primaryKey = 'permission_role_id';
	public $timestamps = false;

	protected $casts = [
		'permission' => 'int',
		'role' => 'int'
	];

	public function permission()
	{
		return $this->belongsTo(\App\Models\Permission::class, 'permission');
	}

	public function role()
	{
		return $this->belongsTo(\App\Models\Role::class, 'role');
	}
}
