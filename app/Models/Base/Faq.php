<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 10 Oct 2019 19:11:47 -0300.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Faq
 * 
 * @property int $faq_id
 * @property string $question
 * @property string $answer
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models\Base
 */
class Faq extends Eloquent
{
	protected $primaryKey = 'faq_id';
}
