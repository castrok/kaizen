<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 06 Aug 2019 14:50:23 -0300.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Draft
 * 
 * @property int $draft_id
 * @property float $draft_value
 * @property int $account
 * @property int $bank_account
 * @property string $draft_proof
 * @property string $draft_informative
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 *
 * @package App\Models\Base
 */
class Draft extends Eloquent
{
	protected $primaryKey = 'draft_id';

	protected $casts = [
		'draft_value' => 'float',
		'account' => 'int',
		'bank_account' => 'int'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class, 'account');
	}

	public function bank_account()
	{
		return $this->belongsTo(\App\Models\BankAccount::class, 'bank_account');
	}
}
