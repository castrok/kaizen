<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 30 Jul 2019 16:52:05 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserAdress
 *
 * @property int $adresse_id
 * @property int $user_id
 * @property string $zip_code
 * @property string $city
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @property \App\Models\Permission $permission
 *
 * @package App\Models\Base
 */
class UserAdress extends Eloquent{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $primaryKey = 'adresse_id';

	protected $casts = [
		'user_id' => 'int'
	];

	public function permission()
	{
		return $this->belongsTo(\App\Models\Permission::class, 'user_id');
	}
}
