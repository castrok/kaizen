<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 31 May 2019 21:25:56 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Plant
 * 
 * @property int $plant_id
 * @property int $owner
 * @property string $plant_name
 * @property string $plant_informative
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $configs
 *
 * @package App\Models\Base
 */
class Plant extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $primaryKey = 'plant_id';

	protected $casts = [
		'owner' => 'int'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'owner');
	}

	public function configs()
	{
		return $this->hasMany(\App\Models\Config::class, 'plant');
	}
}
