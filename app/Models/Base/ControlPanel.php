<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 31 May 2019 21:25:20 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ControlPanel
 * 
 * @property int $control_panel_id
 * @property int $owner
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models\Base
 */
class ControlPanel extends Eloquent
{
	protected $primaryKey = 'control_panel_id';

	protected $casts = [
		'owner' => 'int'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'owner');
	}
}
