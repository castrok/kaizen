<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 02 Aug 2019 16:00:35 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Permission
 * 
 * @property int $permission_id
 * @property string $permission_name
 * @property string $permission_informative
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $roles
 * @property \Illuminate\Database\Eloquent\Collection $user_adresses
 *
 * @package App\Models\Base
 */
class Permission extends Eloquent
{
	protected $primaryKey = 'permission_id';

	public function roles()
	{
		return $this->belongsToMany(\App\Models\Role::class, 'permission_roles', 'permission', 'role')
					->withPivot('permission_role_id');
	}

	public function user_adresses()
	{
		return $this->hasMany(\App\Models\UserAdress::class, 'user_id');
	}
}
