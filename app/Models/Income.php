<?php

namespace App\Models;

class Income extends \App\Models\Base\Income
{
	protected $fillable = [
		'income_value',
		'account',
		'income_informative'
	];
}
