<?php


namespace App\services;
use App\Models\Income;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class IncomesService {

    /**
     * Display a listing of the resource.
     * @Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     * @return Response
     */
    public static function show_authenticated () {
        try {
            $incomes = Income::query()->select(['incomes.*'])
                ->join('accounts','accounts.account_id','incomes.account')
                ->where('accounts.titre', Auth::id())->orderBy('incomes.income_id','ASC')->get();
        }
        catch (Exception $exception) {
            report($exception);
        }

        finally {
            return $incomes;
        }
    }

    public static function show_by_account ($id) {
        try {
            $incomes = Income::query()->select(['incomes.*'])
                ->join('accounts','accounts.account_id','incomes.account')
                ->where('accounts.account_id', $id)->orderBy('incomes.income_id','ASC')->get();
        }
        catch (Exception $exception) {
            report($exception);
        }

        finally {
            return $incomes;
        }
    }

    public static function show($id) {
        try {
            $income = Income::find($id);
        }

        catch (Exception $exception) {
            report($exception);
        }

        finally {
            return $income;
        }
    }
}
