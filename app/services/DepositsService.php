<?php


namespace App\services;

use App\Models\Deposit;
use Auth;
use Exception;
use Illuminate\Http\Response;

class DepositsService {

    public static function show_authenticated () {
        try {
            $deposits = Deposit::query()->select(['deposits.*'])
                ->join('accounts','accounts.account_id','deposits.account')
                ->where('accounts.titre', Auth::id())->orderBy('deposits.deposit_id','ASC')->get();
        }
        catch (Exception $exception) {
            report($exception);
        }

        return $deposits;
    }

    public static function show_by_account ($id) {
        try {
            $deposits = Deposit::query()->select(['deposits.*'])
                ->join('accounts','accounts.account_id','deposits.account')
                ->where('accounts.account_id', $id)->orderBy('deposits.deposit_id','ASC')->get();
        }
        catch (Exception $exception) {
            report($exception);
        }

        finally {
            return $deposits;

        }
    }

    public static function show($id) {
        try {
            $deposit = Deposit::find($id);
        }

        catch (Exception $exception) {
            report($exception);
        }

        finally {
            return $deposit;
        }
    }
}
