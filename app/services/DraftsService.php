<?php


namespace App\services;


use App\Models\Draft;
use Auth;
use Exception;

class DraftsService {

    public static function show_authenticated () {
        try {
            $drafts = Draft::query()->select(['drafts.*'])
                ->join('accounts','accounts.account_id','drafts.account')
                ->where('accounts.titre', Auth::id())->orderBy('drafts.draft_id','ASC')->get();
        }
        catch (Exception $exception) {
            report($exception);
        }

        finally {
            return $drafts;
        }
    }

    public static function show_by_account ($id) {
        try {
            $drafts = Draft::query()->select(['drafts.*'])
                ->join('accounts','accounts.account_id','drafts.account')
                ->where('accounts.account_id', $id)->orderBy('drafts.draft_id','ASC')->get();
        }
        catch (Exception $exception) {
            report($exception);
        }

        finally {
            return $drafts;
        }
    }

    public static function show($id) {
        try {
            $draft = Draft::find($id);
        }

        catch (Exception $exception) {
            report($exception);
        }

        finally {
            return $draft;
        }
    }
}
