<?php


namespace App\services;
use App\Models\Account;
use Auth;
use Exception;
use Illuminate\Http\Response;

class AccountsService {

    /**
     * Display a listing of the resource.
     * @Gilson Vieira Castro Júnior <castrok@live.com (61) 9 9209-6171>
     * @return Response
     */
    public static function show_authenticated() {
        try {
            $account = Account::where('titre', Auth::id())->firstOrFail();
        }

        catch (Exception $exception) {
            report($exception);
        }

        finally {
            return $account;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public static function show($id) {
        try {
            $account = Account::findOrFail($id);
        }
        catch (Exception $exception) {
            report($exception);
        }
        finally {
            return $account;
        }
    }
}
