<?php

namespace App\Providers;

use App\Models\Permission;
use App\Models\User;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @param GateContract $gate
     * @return void
     */
    public function boot(GateContract $gate) {

        $this->registerPolicies($gate);

        $permissions = Permission::with('roles')->get();

        foreach ($permissions as $permission){
            Gate::define($permission->permission_name, function(User $user) use ($permission){
                return $user->hasPermission($permission);
            });
        }

        Gate::before(function (User $user, $ability) {
            if ($user->hasAnyRoles('admin'))
                return true;
        });



        $gate->define('admin',function (){
            return Auth::user()->roles->first()->role_id == 1;
        });

        $gate->define('investor',function (){
            return Auth::user()->roles->first()->role_id == 2;
        });

        $gate->define('maintener',function (){
            return Auth::user()->roles->first()->role_id == 3;
        });

    }
}
