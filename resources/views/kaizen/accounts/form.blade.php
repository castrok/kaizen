@extends('layouts.Inspina.inspina')
@section('content')
    <div class='panel-heading text-center'><h1><b>Usuários</b></h1></div>
    <div class='panel-body'>
        @if($_SERVER['REQUEST_URI'] === '/accounts/create' && empty($accounts->account_id))
            <form method='POST' action='{{route('accounts.store')}}' enctype='multipart/form-data' accept-charset='UTF-8' autocomplete='off' data-toggle='validator'>
                @else
                    <form method='POST' action='{{route('accounts.update', $accounts->account_id)}}' accept-charset='UTF-8' autocomplete='on' enctype='multipart/form-data' data-toggle='validator'>
                        @method('PATCH') @endif @csrf

                        <div class='form-group row'>
                            <div class='col-sm-12 col-md-12'>
                                <label class='control-label' for='titre'>TITULAR</label>
                                <select id='titre' name='titre' class='form-control' required>
                                    <option readonly>ESCOLHA UM TITULAR PARA ESTA CARTEIRA</option>
                                    @foreach($accounts->titres as $titre)
                                        @if(empty($accounts->titre))
                                            <option value='{{$titre->id}}'>{{$titre->name}}</option>
                                        @else
                                            <option value='{{$titre->id}}' {{($titre->id == $accounts->titre) ? 'selected' : ''}}> {{$titre->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class='form-group row'>
                            <div class='col-sm-12 col-md-12'>
                                <label class='control-label' for='income_tax'>TAXA DE RENDIMENTO</label>
                                <input type='number' class='form-control' name='income_tax' id='income_tax' value='{{$accounts->income_tax}}' placeholder='SE DEIXADO EM BRANCO SERA 5%'>
                            </div>
                        </div>

                        <div class='form-group row'>
                            <div class='col-sm-12 col-md-12'>
                                <label class='control-label' for='balance_atual'>SALDO INICIAL</label>
                                <input type='number' class='form-control' name='balance_atual' id='balance_atual' value='{{$accounts->balance_atual}}' min='100' step='1'>
                            </div>
                        </div>

                        <hr/>

                        <div class='row'>
                            <div class='col-sm-12 col-md-12 text-center'>
                                <button class='btn btn-success btn-block btn-rounded' type='submit' id='submit'>Salvar</button>
                                <button class='btn btn-warning btn-block btn-rounded' type='reset'>Limpar</button>
                            </div>
                        </div>
                    </form>
            </form>
    </div>
@endsection
