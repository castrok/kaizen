@extends('layouts.Inspina.inspina')
@section('content')
    <div class='panel-heading text-center'>CARTEIRAS DE INVESTIMENTO</div>
    <div class='panel-body'>
        @can('accounts_create', $accounts)
            <div class="row">
                <span class="container">
                    <a class="btn btn-info btn-rounded" href="{{route('accounts.create')}}"><i class="far fa-plus-square"></i>   NOVO CARTEIRA</a>
                </span>
            </div>
        @endcan
        <table class='bs-dataTables table-striped table-bordered table-hover table-condensed'>
            <thead>
            <tr>
                <th class='font-weight-bold text-center'>INFO</th>
                <th class='font-weight-bold text-center'>TITULAR</th>
                <th class='font-weight-bold text-center'>SALDO ATUAL</th>
                <th class='font-weight-bold text-center'>TAXA %</th>
                <th class='font-weight-bold text-center'>AÇÕES</th>
            </tr>
            </thead>
            <tbody>
            @foreach($accounts as $account)
                <tr>
                    <td class='text-center'>
                        <a href='javascript:void(0)'>
                            <i class='fas fa-info-circle fa-2x text-info' {{$account->account_id}}></i>
                        </a>
                    </td>
                    <td class='text-center'>{{$account->titre_name}}</td>
                    <td class='text-center'>R$ {{number_format($account->balance_atual,'2',',','.')}}</td>
                    <td class='text-center'>{{$account->income_tax}} %</td>
                    <td class='text-center'>
                        <a href='{{route('wallet.show', $account->account_id)}}'>
                            <i class="fas fa-piggy-bank fa-2x text-info"></i>
                        </a>
                        <a href='{{route('accounts.edit',$account->account_id)}}'>
                            <i class='fas fa-edit fa-2x text-success'></i>
                        </a>
                        <a href='javascript:void(0)' id='deletar' data-url="/api/accounts/{{$account->account_id}}">
                            <i class='fas fa-trash-alt fa-2x text-danger'></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
