@extends('layouts.Inspina.inspina')
@section('content')
    <div class='panel-heading text-center'>CONTAS BANCÁRIAS</div>
    <div class='panel-body'>
        @can('bankacc_create',$bankaccounts)
            <div class="row">
                <span class="container">
                    <a class="btn btn-info btn-rounded" href="{{route('bankacc.create')}}"><i class="fas fa-user-plus"></i>   NOVO CONTA BANCÁRIA</a>
                </span>
            </div>
        @endcan
        @can('bankacc_index',$bankaccounts)
            <table class='bs-dataTables table-striped table-bordered table-hover table-condensed'>
                <thead>
                <tr>
                    <th class='font-weight-bold text-center'>INFO</th>
                    <th class='font-weight-bold text-center'>TITULAR</th>
                    <th class='font-weight-bold text-center'>AGÊNCIA</th>
                    <th class='font-weight-bold text-center'>Nº DA CONTA</th>
                    <th class='font-weight-bold text-center'>BANCO</th>
                    <th class='font-weight-bold text-center'>TIPO</th>
                    <th class='font-weight-bold text-center'>AÇÕES</th>
                </tr>
                </thead>
                <tbody>
                @foreach($bankaccounts as $bankaccount)
                    <tr>
                        <td class='text-center'>
                            @can('bankacc_show',$bankaccount)
                                <a href='javascript:void(0)'>
                                    <i class='fas fa-info-circle fa-2x text-info'></i>
                                </a>
                            @endcan
                        </td>
                        <td class='text-center'>{{$bankaccount->bank_acc_titre}}</td>
                        <td class='text-center'>{{$bankaccount->agency}}</td>
                        <td class='text-center'>{{$bankaccount->account_number}}</td>
                        <td class='text-center text-uppercase'>{{$bankaccount->bank_name}}</td>
                        <td class='text-center'>{{$bankaccount->account_type}}</td>
                        <td class='text-center'>
                            @can('bankacc_edit',$bankaccount)
                                <a href='{{route('bankacc.edit',$bankaccount->bank_account_id)}}'>
                                    <i class='fas fa-edit fa-2x text-success'></i>
                                </a>
                            @endcan
                            @can('bankacc_destroy',$bankaccount)
                                <a href='{{route('bankacc.destroy',$bankaccount->bank_account_id)}}'>
                                    <i class='fas fa-trash-alt fa-2x text-danger'></i>
                                </a>
                            @endcan
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endcan
    </div>
@endsection
