@extends('layouts.Inspina.inspina')
@section('content')
    <div class='panel-heading text-center font-bold'>CONTAS BANCÁRIAS</div>
    <div class='panel-body'>
        @if($_SERVER['REQUEST_URI'] === '/bankacc/create' && empty($bankaccounts->bank_account_id))
            <form method='POST' action='{{route('bankacc.store')}}' accept-charset='UTF-8' autocomplete='on' data-toggle='validator'>
                @else
                    <form method='POST' action='{{route('bankacc.update', $bankaccounts->bank_account_id)}}' accept-charset='UTF-8' autocomplete='on' data-toggle='validator'>
                        @method('PATCH') @endif @csrf

                        <div class='form-group row'>
                            <div class='col-sm-12 col-md-12'>
                                <label class='control-label' for='bank_account_titre'>TITULAR</label>
                                <select id='bank_account_titre' name='bank_account_titre' class='form-control' required>
{{--                                    @can('investor')--}}
{{--                                        <option value='{{Auth::id()}}' selected> {{Auth::user()->name}}</option>--}}
{{--                                    @endcan--}}
{{--                                    @cannot('investor')--}}
                                        <option readonly>ESCOLHA UM TITULAR PARA ESTA CONTA BANCÁRIA</option>
                                        @foreach($bankaccounts->bank_acc_titre as $bank_acc_titre)
                                            @if(empty($bankaccounts->bank_acc_titre))
                                                <option value='{{$bank_acc_titre->id}}'>{{$bank_acc_titre->name}}</option>
                                            @else
                                                <option value='{{$bank_acc_titre->id}}' {{ ($bank_acc_titre->id === $bankaccounts->bank_account_titre) ? 'selected ' : ''}}> {{$bank_acc_titre->name}}</option>
                                            @endif
                                        @endforeach
{{--                                    @endcannot--}}
                                </select>
                            </div>
                        </div>

                        <div class='form-group row'>
                            <div class='col-sm-12 col-md-4'>
                                <label class='control-label' for='agency'>AGÊNCIA</label>
                                <input type='number' class='form-control' name='agency' id='agency' value='{{$bankaccounts->agency}}' data-error='SOMENTE NÚMEROS' required>
                                <div class='help-block with-errors'></div>
                            </div>

                            <div class='col-sm-12 col-md-4'>
                                <label class='control-label' for='account_number'>NÚMERO DA CONTA</label>
                                <input type='number' class='form-control' name='account_number' id='account_number' value='{{$bankaccounts->account_number}}' required>
                            </div>

                            <div class='col-sm-12 col-md-4'>
                                <label for="account_type">TIPO DE CONTA</label>
                                <select name="account_type" id="account_type" class='form-control' required>
                                    <option value="CC">CONTA CORRENTE</option>
                                    <option value="CP">CONTA POUPANÇA</option>
                                </select>
                            </div>
                        </div>

                        <div class='form-group row'>
                            <div class='col-sm-12 col-md-12'>
                                <label class='control-label' for='bank_name'>NOME DO BANCO</label>
                                <input type='text' class='form-control' name='bank_name' id='bank_name' value='{{$bankaccounts->bank_name}}' required>
                            </div>
                        </div>

                        <hr/>

                        <div class='row'>
                            <div class='col-sm-12 col-md-12 text-center'>
                                <button class='btn btn-success btn-block btn-rounded' type='submit' id='submit'>Salvar</button>
                                <button class='btn btn-warning btn-block btn-rounded' type='reset'>Limpar</button>
                            </div>
                        </div>
                    </form>
            </form>
    </div>
@endsection
