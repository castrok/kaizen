@extends('layouts.Inspina.inspina')
@section('content')
    <div class='panel-heading text-center'>Usuário</div>
    <div class='panel-body'>
        @if($_SERVER['REQUEST_URI'] === '/users/create' && empty($users->id))
            <form method='POST' action='{{route('users.store')}}' enctype='multipart/form-data' accept-charset='UTF-8' autocomplete='off' data-toggle='validator'>
                @else
                    <form method='POST' action='{{route('users.update', $users->id)}}' accept-charset='UTF-8' autocomplete='on' enctype='multipart/form-data' data-toggle='validator'>
                        @method('PATCH') @endif @csrf

                        <div class='form-group row'>
                            <div class='col-sm-12 col-md-12'>
                                <label class='control-label' for='name'>Nome</label>
                                <input type='text' class='form-control' name='name' id='name' value='{{$users->name}}' data-error='Insira um nome válido' required>
                                <div class='help-block with-errors'></div>
                            </div>
                        </div>

                        <div class='form-group row'>
                            <div class='col-sm-12 col-md-12'>
                                <label class='control-label' for='email'>E-Mail</label>
                                <input type='text' class='form-control' name='email' id='email' value='{{$users->email}}' data-error='Insira um email válido' required>
                                <div class='help-block with-errors'></div>
                            </div>
                        </div>

                        <div class='form-group row'>
                            <div class='col-sm-12 col-md-6'>
                                <label class='control-label' for='rg'>RG</label>
                                <input type='number' class='form-control' name='rg' id='rg' data-minlength='5' value="{{$users->rg}}">
                                <div class='help-block'>Somente números</div>
                            </div>

                            <div class='col-sm-12 col-md-6'>
                                <label class='control-label' for='cpf'>CPF</label>
                                <input type='text' class='form-control' id='cpf' name='cpf' data-minlength='14' value="{{$users->cpf}}">
                                <div class='help-block with-errors'></div>
                            </div>
                        </div>

                        <div class="panel-group" id="accordion">
                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                            SENHA DE ACESSO</a>
                                    </h4>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class='form-group row'>
                                            <div class='col-sm-12 col-md-6'>
                                                <label class='control-label' for='password'>Senha</label>
                                                <input type='password' class='form-control' name='password' id='password' data-minlength='8'>
                                                <div class='help-block'>Minimo 8 caracteres</div>
                                            </div>

                                            <div class='col-sm-12 col-md-6'>
                                                <label class='control-label' for='password_confirm'>Confirmar Senha</label>
                                                <input type='password' class='form-control' id='password_confirm' data-match='#password' data-match-error='As senhas não são iguais'>
                                                <div class='help-block with-errors'></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr/>

                        <div class='row'>
                            <div class='col-sm-12 col-md-12 text-center'>
                                <button class='btn btn-success btn-block btn-rounded' type='submit' id='submit'>Salvar</button>
                                <button class='btn btn-warning btn-block btn-rounded' type='reset'>Limpar</button>
                            </div>
                        </div>
                    </form>
            </form>
    </div>
@endsection
