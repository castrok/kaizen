@extends('layouts.Inspina.inspina')
@section('content')
    <div class='panel-heading text-center'>USUÁRIOS</div>
    <div class='panel-body'>
        @can('users_create',$users)
            <div class="row">
                <span class="container">
                    <a class="btn btn-info btn-rounded" href="{{route('users.create')}}" data-toggle="tooltip" title="ADICIONAR USUÁRIO"><i class="fas fa-user-plus"></i>   NOVO USUÁRIO</a>
                </span>
            </div>
        @endcan
        @can('users_index',$users)
            <table class='bs-dataTables table-striped table-bordered table-hover table-condensed'>
                <thead>
                <tr>
                    <th class='font-weight-bold text-center'>INFO</th>
                    <th class='font-weight-bold text-center'>NOME</th>
                    <th class='font-weight-bold text-center'>EMAIL</th>
                    <th class='font-weight-bold text-center'>RG</th>
                    <th class='font-weight-bold text-center'>CPF</th>
                    <th class='font-weight-bold text-center'>AÇÕES</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td class='text-center'>
                            @can('users_show',$user)
                                <a href='javascript:void(0)' id="user_info" data-id="{{$user->id}}" data-toggle="tooltip" title="DETALHES">
                                    <i class='fas fa-info-circle fa-2x text-info'></i>
                                </a>
                            @endcan
                        </td>
                        <td class='text-center'>{{$user->name}}</td>
                        <td class='text-center'>{{$user->email}}</td>
                        <td class='text-center'>{{$user->rg}}</td>
                        <td class='text-center'>{{$user->cpf}}</td>
                        <td class='text-center'>
                            @can('users_edit',$user)
                                <a href='{{route('users.edit',$user->id)}}' data-toggle="tooltip" title="EDITAR">
                                    <i class='fas fa-edit fa-2x text-success'></i>
                                </a>
                            @endcan
                            @can('users_destroy',$user)
                                <a href='javascript:void(0)' data-toggle="tooltip" title="REMOVER">
                                    <i class='fas fa-trash-alt fa-2x text-danger'></i>
                                </a>
                            @endcan
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endcan
    </div>
@endsection
