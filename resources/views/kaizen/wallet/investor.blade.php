@extends('layouts.Inspina.inspina')
@section('content')
    <meta name='account_id' content='{{$accounts->account_id}}' />

    <div class='panel-body solitude'>
        <div class="row">

            <h1 class="text-center">{{$accounts->titre_name}}</h1>

            <div class="col-md-3 col-xs-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-success pull-right">ATUAL</span>
                        <h5>Saldo total</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">R$ {{number_format($accounts->balance_atual,'2','.',',')}}</h1>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-info pull-right">MENSAL</span>
                        <h5>Taxa</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">{{number_format($accounts->income_tax,'2','.',',')}} %</h1>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-xs-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-success pull-right">PREVISTO</span>
                        <h5>Rendimento</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">R$ {{number_format($accounts->balance_expected,'2','.',',')}}</h1>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-success pull-right">PREVISTO</span>
                        <h5>Saldo proximo mês</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">R$ {{number_format($accounts->next_month_total_ballance_expected,'2',',','.')}}</h1>
                    </div>
                </div>
            </div>
        </div> {{--SALDOS--}}

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-md-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div>
                                <canvas id="lineChart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> {{--GRÁFICOS--}}

        <div class="row">
            <div class="col-md-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#deposits" aria-expanded="false"><i class="fa fa-laptop"></i>DEPÓSITOS</a></li>
                        <li class=""><a data-toggle="tab" href="#drafts" aria-expanded="true"><i class="fa fa-desktop"></i>SAQUES</a></li>
                        <li class=""><a data-toggle="tab" href="#incomes" aria-expanded="true"><i class="fa fa-desktop"></i>RENDIMENTOS</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="deposits" class="tab-pane active">
                            <div class="panel-body">
                                <table class='bs-dataTables table-striped table-bordered table-hover table-condensed'>
                                    <thead>
                                    <tr>
                                        <th class='font-weight-bold text-center'>INFO</th>
                                        <th class='font-weight-bold text-center'>VALOR</th>
                                        <th class='font-weight-bold text-center'>DATA</th>
                                        <th class='font-weight-bold text-center'>COMPROVANTE</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($deposits as $deposit)
                                        <tr>
                                            <td class='text-center'>
                                                <a href='javascript:void(0)' id="deposit_info" data-deposit_id="{{$deposit->deposit_id}}">
                                                    <i class='fas fa-info-circle fa-2x text-info'></i>
                                                </a>
                                            </td>
                                            <td class='text-center'>R$ {{number_format($deposit->deposit_value,'2','.',',')}}</td>
                                            <td class='text-center'>{{date_format($deposit->created_at, 'd/m/Y')}}</td>
                                            <td class='text-center'>
                                                <a href='javascript:void(0)' id='receipt' data-show_proof='{{asset('storage/'.$deposit->deposit_proof)}}'><i class="far fa-file-image fa-2x"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div id="drafts" class="tab-pane">
                            <div class="panel-body">
                                <table class='bs-dataTables table-striped table-bordered table-hover table-condensed'>
                                    <thead>
                                    <tr>
                                        <th class='font-weight-bold text-center'>INFO</th>
                                        <th class='font-weight-bold text-center'>VALOR</th>
                                        <th class='font-weight-bold text-center'>DATA</th>
                                        <th class='font-weight-bold text-center'>COMPROVANTE</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($drafts as $draft)
                                        <tr>
                                            <td class='text-center'>
                                                <a href='javascript:void(0)' id="draft_info" data-draft_id="{{$draft->draft_id}}">
                                                    <i class='fas fa-info-circle fa-2x text-info'></i>
                                                </a>
                                            </td>
                                            <td class='text-center'>R$ {{number_format($draft->draft_value, '2','.',',')}}</td>
                                            <td class='text-center'>{{date_format($draft->created_at, 'd/m/Y')}}</td>
                                            <td class='text-center'>
                                                <a href='javascript:void(0)' id='receipt' data-show_proof='{{asset('storage/'.$draft->draft_proof)}}'><i class="far fa-file-image fa-2x"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div id="incomes" class="tab-pane">
                            <div class="panel-body">
                                <table class='bs-dataTables table-striped table-bordered table-hover table-condensed'>
                                    <thead>
                                    <tr>
                                        <th class='font-weight-bold text-center'>INFO</th>
                                        <th class='font-weight-bold text-center'>VALOR</th>
                                        <th class='font-weight-bold text-center'>DATA</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($incomes as $income)
                                        <tr>
                                            <td class='text-center'>
                                                <a href='javascript:void(0)' id="income_info" data-income_id="{{$income->income_id}}">
                                                    <i class='fas fa-info-circle fa-2x text-info'></i>
                                                </a>
                                            </td>
                                            <td class='text-center'>R$ {{number_format($income->income_value, '2','.',',')}}</td>
                                            <td class='text-center'>{{date_format($income->created_at,'d/m/Y')}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> {{--MOVIMENTAÇÕES--}}
    </div>
@endsection
