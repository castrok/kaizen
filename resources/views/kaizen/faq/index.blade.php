@extends('layouts.Inspina.inspina')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInRight">

                <div class="ibox-content m-b-sm border-bottom">
                    <div class="text-center p-lg">
                        <h2>Caso não encontre a resposta para sua duvida, adicione-a clicando</h2>
                        {{--TRIGGER THE MODAL WITH A BUTTON--}}
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create_faq"><i class="fa fa-plus"></i><span class="bold">AQUI</span></button>
                    </div>
                </div>

                <div class="faq-item">
                    <div class="row">
                        <div class="col-md-7">
                            <a data-toggle="collapse" href="#faq1" class="faq-question">What It a long established fact that a reader ?</a>
                            <small>Adicionada em <i class="fa fa-clock-o"></i> <strong> {{date(('d/m/Y H:i:s'))}} </strong>  </small>
                        </div>
                        {{--<div class="col-md-3">
                            <span class="small font-bold">Robert Nowak</span>
                            <div class="tag-list">
                                <span class="tag-item">General</span>
                                <span class="tag-item">License</span>
                            </div>
                        </div>
                        <div class="col-md-2 text-right">
                            <span class="small font-bold">Voting </span><br/>
                            42
                        </div>--}}
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="faq1" class="panel-collapse collapse ">
                                <div class="faq-answer">
                                    <p>
                                        It is a long established fact that a reader will be distracted by the
                                        readable content of a page when looking at its layout. The point of
                                        using Lorem Ipsum is that it has a more-or-less normal distribution of
                                        letters, as opposed to using 'Content here, content here', making it
                                        look like readable English.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--<div class="faq-item">
                    <div class="row">
                        <div class="col-md-7">
                            <a data-toggle="collapse" href="#faq2" class="faq-question">Many desktop publishing packages ?</a>
                            <small>Added by <strong>Mark Nowak</strong> <i class="fa fa-clock-o"></i> Today 3:30 pm - 11.06.2014</small>
                        </div>
                        <div class="col-md-3">
                            <span class="small font-bold">Robert Nowak</span>
                            <div class="tag-list">
                                <span class="tag-item">General</span>
                                <span class="tag-item">License</span>
                                <span class="tag-item">CC</span>
                            </div>
                        </div>
                        <div class="col-md-2 text-right">
                            <span class="small font-bold">Voting </span><br/>
                            24
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="faq2" class="panel-collapse collapse">
                                <div class="faq-answer">
                                    <p>
                                        Many desktop publishing packages and web page editors now use Lorem
                                        Ipsum as their default model text, and a search for 'lorem ipsum' will
                                        uncover many web sites still in their infancy. Various versions have
                                        evolved over the years, sometimes by accident, sometimes on purpose
                                        (injected humour and the like).
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--}}
            </div>
        </div>
    </div>

    {{--MODAL--}}
    <div id="create_faq" class="modal fade" role="dialog">
        <div class="modal-dialog">

            {{--MODAL CONTENT--}}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Insira sua duvida no campo abaixo</h4>
                </div>
                <form method="post" action="{{route('faq.store')}}" accept-charset="UTF-8" AUTOCOMPLETE="off">
                    <div class="modal-body">
                        @csrf
                        <div class='form-group row'>
                            <div class='col-sm-12 col-md-12'>
                                <label for="question"></label>
                                <textarea name="question" id="question" class='form-control' cols="20" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">FECHAR</button>
                        <button type="reset" class="btn btn-warning" >LIMPAR</button>
                        <button type="submit" class="btn btn-info">ENVIAR</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection
