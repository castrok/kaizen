@extends('layouts.Inspina.inspina')
@section('content')
    <div class='panel-heading text-center'>DEPÓSITOS</div>
    <div class='panel-body'>
        @can('deposits_create',$deposits)
            <div class="row">
                <span class="container">
                    <a class="btn btn-info btn-rounded" href="{{route('deposits.create')}}"><i class="fas fa-user-plus"></i>   NOVO DEPÓSITO</a>
                </span>
            </div>
        @endcan
        @can('deposits_index',$deposits)
            <table class='bs-dataTables table-striped table-bordered table-hover table-condensed'>
                <thead>
                <tr>
                    <th class='font-weight-bold text-center'>INFO</th>
                    <th class='font-weight-bold text-center'>TITULAR</th>
                    <th class='font-weight-bold text-center'>VALOR</th>
                    <th class='font-weight-bold text-center'>DATA</th>
                    <th class='font-weight-bold text-center'>AÇÕES</th>
                </tr>
                </thead>
                <tbody>
                @foreach($deposits as $deposit)
                    <tr>
                        <td class='text-center'>
                            @can('deposits_show',$deposit)
                                <a href='javascript:void(0)'>
                                    <i class='fas fa-info-circle fa-2x text-info'></i>
                                </a>
                            @endcan
                        </td>
                        <td class='text-center'>{{$deposit->titre_name}}</td>
                        <td class='text-center'>
                            @can('deposits_edit',$deposit)
                                <a href='{{route('deposits.edit',$deposit->deposit_id)}}'>
                                    <i class='fas fa-edit fa-2x text-success'></i>
                                </a>
                            @endcan
                            @can('deposits_destroy',$deposit)
                                <a href='{{route('deposits.destroy', $deposit->deposit_id)}}'>
                                    <i class='fas fa-trash-alt fa-2x text-danger'></i>
                                </a>
                            @endcan
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endcan
    </div>
@endsection
