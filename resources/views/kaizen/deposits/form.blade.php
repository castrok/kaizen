@extends('layouts.Inspina.inspina')
@section('content')
    <div class='panel-heading text-center'>DEPÓSITOS</div>
    <div class='panel-body'>
        @if($_SERVER['REQUEST_URI'] === '/deposits/create' && empty($deposits->id))
            <form method='POST' action='{{route('deposits.store')}}' enctype='multipart/form-data' accept-charset='UTF-8' autocomplete='on' data-toggle='validator'>
                @else
                    <form action='{{route('deposits.update', $deposits->deposit_id)}}' accept-charset='UTF-8' autocomplete='on' enctype='multipart/form-data' data-toggle='validator'>
                        @method('PATCH') @endif @csrf

                        <div class='form-group row'>
                            <div class='col-sm-12 col-md-12'>
                                <label class='control-label' for='deposit_value'>Valor</label>
                                <input type='number' class='form-control' name='deposit_value' id='deposit_value' value='{{$deposits->deposit_value}}' data-error='Insira um valor válido' required min="100" step="0.01" placeholder="MINIMO R$ 100,00">
                                <div class='help-block with-errors'></div>
                            </div>
                        </div>

                        <div class='form-group row'>
                            <div class='col-sm-12 col-md-12'>
                                <label class='control-label' for='account'>Conta</label>
                                <select name="account" id="account" class='form-control' required data-error='Insira um valor válido'>
                                    @forelse($accounts as $account)
                                        <option value="{{$account->account_id}}">{{$account->titre_name}}</option>
                                    @empty
                                        <option>NÃO FORAM ENCONTRADAS CONTAS</option>
                                    @endforelse
                                </select>
                                <div class='help-block with-errors'></div>
                            </div>
                        </div>

                        <div class='form-group row'>
                            <div class='col-sm-12 col-md-12'>
                                <label for='photo'>Comprovante de depósito</label>
                                <input type='file' class='form-control-file' id='deposit_proof' name='deposit_proof' required>
                            </div>
                        </div>

                        <hr/>

                        <div class='row'>
                            <div class='col-sm-12 col-md-12 text-center'>
                                <button class='btn btn-success btn-block btn-rounded' type='submit' id='submit'>Salvar</button>
                                <button class='btn btn-warning btn-block btn-rounded' type='reset'>Limpar</button>
                            </div>
                        </div>
                    </form>
            </form>
    </div>
@endsection
