<!DOCTYPE html>
<html lang='pt br'>
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta name='msapplication-tap-highlight' content='no'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>

    <base content='{{url('/')}}'  id='base' target='_self'/> {{--BASE URL FOR AJAX REQUESTS--}}
    <meta name='csrf-token' content='{{csrf_token()}}' /> {{--CSRF Token--}}

    <meta name='description' content='' />
    <meta name='keywords' content='' />
    <title>Kaizen KZN | 0.1v </title>

    <link rel='stylesheet' type='text/css' href='{{asset('css/cores/bootstrap.min.css')}}' />
    <link rel='stylesheet' type='text/css' href='{{asset('css/cores/sweetalert2.min.css')}}' />
    <link rel='stylesheet' type='text/css' href='{{asset('css/cores/inspina.min.css')}}' />
    <link rel='stylesheet' type='text/css' href='{{asset('css/plugins/DataTables/datatables.min.css')}}' />
{{--    <link rel='stylesheet' type='text/css' href='{{asset('css/cores/customInspina.min.css')}}' />--}}
    <link rel='stylesheet' type='text/css' href='{{asset('sources/css/cores/customInspina.css')}}' />
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.8.2/css/all.css' integrity='sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay' crossorigin='anonymous'>
</head>
<body>
{{--WRAPPER--}}
<div id='wrapper'>
    <div class="se-pre-con"></div>

    {{--NAVIGATION--}}
    @include('layouts.Inspina.navigation') {{--END NAVIGATION--}}

    {{--PAGE WRAPER--}}
    <div id='page-wrapper' class='gray-bg'>

        {{--PAGE WRAPPER--}}
        @include('layouts.Inspina.topnavbar') {{--END PAGE WRAPPER--}}

        {{--FOOTER--}}
        @include('layouts.Inspina.footer') {{--END FOOTER--}}


        <div class='row'>
            <div class='panel panel-default'>
                <div class='panel-body'>
                    <div class='panel panel-primary'>
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>

    </div>{{--END PAGE WRAPPER--}}
</div>{{--END WRAPPER--}}

<script type='text/javascript' src='{{asset('js/cores/jquery-3.4.1.min.js')}}'></script> {{--JQUERY CORE--}}
<script type='text/javascript' src='{{asset('js/cores/bootstrap.min.js')}}'></script> {{--BOOTSTRAP CORE--}}
<script type='text/javascript' src='{{asset('js/cores/sweetalert2.min.js')}}'></script> {{--SWEET ALERT--}}
<script type='text/javascript' src='{{asset('js/cores/inspina.min.js')}}'></script> {{--TEMPLATE CORE--}}

<script type='text/javascript' src='{{asset('js/plugins/chartJs/Chart.min.js')}}' defer></script> {{--CHART CORE--}}
<script type='text/javascript' src='{{asset('js/plugins/DataTables/datatables.min.js')}}' defer></script> {{--DATA TABLES--}}
<script type='text/javascript' src='{{asset('js/plugins/bootstrapValidator.min.js')}}' defer></script> {{--INPUT VALIDATOR--}}
<script type='text/javascript' src='{{asset('js/plugins/maskedinput.min.js')}}' defer></script> {{--INPUT MASK--}}
<script type='text/javascript' src='{{asset('js/plugins/jquerydateformat/JqueryDateFormat.min.js')}}' defer></script> {{--JQUERY DATE FORMAT--}}
{{--<script type='text/javascript' src='{{asset('js/cores/customInspina.min.js')}}' defer></script>--}}
<script type='text/javascript' src='{{asset('sources/js/cores/customInspina.js')}}' defer></script>

</body>
</html>
