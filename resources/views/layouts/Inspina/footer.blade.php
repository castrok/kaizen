<footer class="footer">
    <div class="pull-right">
        Developed By <strong>Akheem CK.</strong>
    </div>
    <div>
        <strong>Kaizen &copy;</strong>  2019-{{date('Y')}}
    </div>
</footer>
