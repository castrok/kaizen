<div class='row border-bottom'>
    <nav class='navbar navbar-static-top white-bg' role='navigation'>
        <div class='navbar-header'>
            <a class='navbar-minimalize minimalize-styl-2 btn btn-primary ' href='javascript:void(0)'><i class='fa fa-bars'></i></a>
        </div>
        <ul class='nav navbar-top-links navbar-right text-right'>
            @can('admin')
                <li>
                    <a href='{{route('log-viewer::dashboard')}}' target="_blank">
                        <i class="fas fa-file-code fa-2x"></i>
                    </a>
                </li>
            @endcan
            <li>
                <a href='{{route('users.edit', Auth::id())}}'>
                    <i class="fas fa-user-edit fa-2x"></i>
                </a>
            </li>
            <li>
                <a href='javascript:void(0)' id='logoff'>
                    <i class='fas fa-sign-out-alt fa-2x'></i>
                </a>
            </li>
        </ul>
    </nav>
</div>
