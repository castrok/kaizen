<nav class='navbar-default navbar-static-side' role='navigation'>
    <div class='sidebar-collapse'>
        <ul class='nav metismenu' id='side-menu'>
            <li class='nav-header'>

                {{--<div class="dropdown profile-element">
                    <img alt="image" class="rounded-circle" src="img/profile_small.jpg">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="block m-t-xs font-bold">David Williams</span>
                        <span class="text-muted text-xs block">Art Director <b class="caret"></b></span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a class="dropdown-item" href="profile.html">Profile</a></li>
                        <li><a class="dropdown-item" href="contacts.html">Contacts</a></li>
                        <li><a class="dropdown-item" href="mailbox.html">Mailbox</a></li>
                        <li class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="login.html">Logout</a></li>
                    </ul>
                </div>--}}

                <div class='dropdown profile-element'>
                        <span class='clear'>
                            <span class='block m-t-xs'>
                                <strong class='font-bold text-white'>{{current(str_word_count(Auth::user()->name, 1))}}</strong>
                            </span>
                        </span>
                    <p>{{ucfirst(strtolower((empty(Auth::user()->roles->first()->role_name)) ? '' : Auth::user()->roles->first()->role_informative))}}</p>
                    <ul class='dropdown-menu animated fadeInRight m-t-xs'>
                        <li>
                            <i class='fas fa-sign-out-alt fa-2x'></i> Sair
                        </li>
                    </ul>
                </div>
                <div class='logo-element'>
                    <i class="fas fa-hand-holding-usd"></i>
                </div>
            </li>

            @can('admin')
                @can('users_index')
                    <li data-toggle="tooltip" title="PAINEL USUÁRIOS">
                        <a href='{{route('users.index')}}'><i class='fas fa-users fa-2x'></i> <span class='nav-label text-uppercase'>USUÁRIOS</span></a>
                    </li>
                @endcan

                @can('accounts_index')
                    <li data-toggle="tooltip" title="PAINEL CARTEIRAS">
                        <a href='{{route('accounts.index')}}'><i class='fas fa-wallet fa-2x'></i> <span class='nav-label text-uppercase'>CARTEIRAS</span></a>
                    </li>
                @endcan
            @endcan

            @cannot('admin')
                @can('users_show')
                    <li data-toggle="tooltip" title="MEU PAINEL">
                        <a href='{{route('users.show', Auth::id())}}'><i class='fas fa-users fa-2x'></i> <span class='nav-label text-uppercase'>MEUS DADOS</span></a>
                    </li>
                @endcan
                @can('accounts_index')
                    <li data-toggle="tooltip" title="MINHA CARTEIRA">
                        <a href='{{route('wallet.index')}}'><i class='fas fa-wallet fa-2x'></i> <span class='nav-label text-uppercase'>MINHA CARTEIRA</span></a>
                    </li>
                @endcan
            @endcan

            @can('bankacc_index')
                <li data-toggle="tooltip" title="PAINEL CONTAS">
                    <a href='{{route('bankacc.index')}}'><i class='fas fa-university fa-2x'></i> <span class='nav-label text-uppercase'>CONTAS BANCÁRIAS</span></a>
                </li>
            @endcan

            {{--<li class="landing_link">
                <a target="_blank" href="">
                    <i class="fa fa-database"></i>
                    <span class="nav-label">Landing Page</span>
                    <span class="label label-warning pull-right">NEW</span>
                </a>
            </li>--}}

            <li class="special_link">
                <a href="{{route('faq.index')}}" data-toggle="tooltip" title="PERGUNTAS FREQUENTES">
                    <i class="fas fa-question-circle fa-2x"></i>
                    <span class="nav-label">FAQ</span>
                </a>
            </li>
        </ul>
    </div>
</nav>
