@extends('layouts.app')
@section('content')
    <div class="middle-box text-center animated fadeInDown">
        <h1 class="logo-name">403</h1>
        <h2 class="font-bold">Ops...</h2>

        <div class="error-desc">
            Desculpe. Você não tem permissão para acessar esta área : - /
            <div class="form-inline m-t">
                <a href="{{url('/')}}" type="submit" class="btn btn-primary">Voltar ao início</a>
            </div>
        </div>
    </div>
@endsection
